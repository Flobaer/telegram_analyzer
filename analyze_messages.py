import argparse
import csv
from datetime import datetime
import logging
import os
import sys
from typing import Dict, Iterable, List

import bs4


class DOMElementError(Exception):
    pass


class Message:
    mapping = {'date': 0, 'chat': 1, 'author': 2, 'type': 3, 'content': 4}

    def __init__(self, date=None, chat=None, author=None, type=None, content=None):
        if isinstance(date, str):
            date = datetime.fromisoformat(date)
        self.message = [date, chat, author, type, content]

    def __getitem__(self, key):
        if isinstance(key, str):
            return self.message[self.mapping[key]]
        else:
            return self.message[key]

    def __setitem__(self, key, value):
        if isinstance(key, str):
            index = self.mapping[key]
        else:
            index = key

        self.message[index] = value

    def __str__(self):
        return str(self.message)

    def keys(self):
        return sorted(self.mapping, key=self.mapping.__getitem__)

    def as_dict(self):
        keys = self.keys()
        return {keys[index]: value for (index, value) in enumerate(self.message)}

    def is_valid(self):
        return all(self.message)


def read_messages_from_csv(csv_file_names: List[str]) -> List[Message]:
    messages = []

    for csv_file_name in csv_file_names:
        with open(csv_file_name) as csv_file:
            reader = csv.DictReader(csv_file)
            line_count = 0
            for row in reader:
                if line_count == 0:
                    line_count += 1
                messages.append(Message(**row))
                line_count += 1

    messages.sort(key=lambda x: x['date'])
    return messages


def save_messages_to_csv(messages: List[Message], csv_file_name: str) -> None:
    try:
        if not messages:
            return

        field_names = messages[0].keys()
        with open(args.output, 'x') as f:
            writer = csv.DictWriter(f, fieldnames=field_names)
            writer.writeheader()
            for message in messages:
                writer.writerow(message.as_dict())

    except FileExistsError:
        logging.exception("Error while opening CSV output file for messages. The file '{}' "
                          "already exists and will not automatically be overwritten. Move or "
                          "delete this file manually and try again.".format(args.output))
        print("Error while opening CSV output file for messages. Check log for more details.",
              file=sys.stderr)
        sys.exit(1)


def get_elements(soup: bs4.BeautifulSoup, selector: str, n: str):
    elements = soup.select(selector)

    template = "The root element was:\n{}.".format(soup)
    if n == '1':
        template1 = ("No DOM element was found for the following CSS selector (expected exactly "
                     "one): '{}'.")
        template2 = ("Multiple DOM elements were found for the following CSS selector (expected "
                     "exactly one): '{}'.")
        if len(elements) == 0:
            message = template1.format(selector) + " " + template
            raise DOMElementError(message)
        elif len(elements) > 1:
            message = template2.format(selector) + " " + template
            raise DOMElementError(message)
        else:
            return elements[0]

    elif n == '?':
        template1 = ("Multiple DOM elements were found for the following CSS selector (expected "
                     "one or none): '{}'.")
        if len(elements) > 1:
            message = template1.format(selector) + " " + template
            raise DOMElementError(message)
        else:
            return elements

    elif n == '+':
        template1 = ("No DOM element was found for the following CSS selector (expected at least "
                     "one): '{}'.")
        if len(elements) == 0:
            message = template1.format(selector) + " " + template
            raise DOMElementError(message)
        else:
            return elements


def parse_message_history(html_file_names: Iterable) -> List[Message]:
    raw_html_files = []

    for file_name in html_file_names:
        with open(file_name, 'r') as f:
            raw_html_files.append((file_name, f.read()))

    messages: List[Message] = []

    for file_name, raw_html_text in raw_html_files:
        logging.info("Processing file: {}".format(file_name))
        soup = bs4.BeautifulSoup(raw_html_text, 'html.parser')

        chat_name = []
        last_quote_author = None

        # get name of (group) chat
        chat_name_selector = 'div.page_header > div.content > div.text.bold'
        try:
            chat_name.append(get_elements(soup, chat_name_selector, '1').text.strip())
        except DOMElementError:
            logging.exception("Error while parsing file: {}".format(file_name))
            print("Error while parsing HTML file. Check log for more details.", file=sys.stderr)
            sys.exit(1)

        try:
            history = get_elements(soup, 'div.history', '1')
        except DOMElementError:
            logging.exception("Error while parsing file: {}".format(file_name))
            print("Error while parsing HTML file. Check log for more details.", file=sys.stderr)
            sys.exit(1)

        for message_div in history.select('div.message'):
            text_message = Message()
            quote_message = Message()
            sub_messages = [text_message, quote_message]

            if {'message', 'default', 'clearfix'}.issubset(message_div['class']):
                try:
                    date_text = get_elements(message_div, 'div.body > div.date', '1')['title']
                except DOMElementError:
                    logging.exception("Error while parsing file: {}".format(file_name))
                    print("Error while parsing HTML file. Check log for more details.",
                          file=sys.stderr)
                    sys.exit(1)

                date = datetime.strptime(date_text, "%d.%m.%Y %H:%M:%S")

                try:
                    from_name = get_elements(message_div, 'div[class=body] > div.from_name', '?')
                except DOMElementError:
                    logging.exception("Error while parsing file: {}".format(file_name))
                    print("Error while parsing HTML file. Check log for more details.",
                          file=sys.stderr)
                    sys.exit(1)

                if from_name:
                    author = from_name[0].text.strip()
                else:
                    author = messages[-1]['author']

                text = get_elements(message_div, 'div.text', '?')

                if text:
                    text_message['date'] = date
                    text_message['chat'] = chat_name[-1]
                    text_message['author'] = author
                    text_message['type'] = 'text'
                    text_message['content'] = text[0].text.strip()

                try:
                    quote = get_elements(message_div, 'div[class=body] > div.forwarded.body', '?')
                except DOMElementError:
                    logging.exception("Error while parsing file: {}".format(file_name))
                    print("Error while parsing HTML file. Check log for more details.",
                          file=sys.stderr)
                    sys.exit(1)

                if quote:
                    try:
                        quote_author = get_elements(quote[0], 'div.from_name', '?')
                        quote_date = get_elements(quote[0], 'div.from_name > span.details', '?')
                    except DOMElementError:
                        logging.exception("Error while parsing file: {}".format(file_name))
                        print("Error while parsing HTML file. Check log for more details.",
                              file=sys.stderr)
                        sys.exit(1)

                    if quote_author and quote_date:
                        last_quote_author = (quote_author[0].text.strip()
                                             + quote_date[0].text.strip())

                    quote_message['date'] = date
                    quote_message['chat'] = chat_name[-1]
                    quote_message['author'] = author
                    quote_message['type'] = 'quote'
                    quote_message['content'] = last_quote_author

                for sub_message in sub_messages:
                    if sub_message.is_valid():
                        messages.append(sub_message)

        if len(chat_name) > 1:
            logging.warning("The HTML files seem to belong to different chats, multiple chat "
                            "names were found: {}".format(chat_name))

    messages.sort(key=lambda x: x[0])
    return messages


def analyze_messages(messages: List[Message]) -> Dict[str, Dict[str, int]]:
    analysis: Dict[str, Dict[str, int]] = {}
    for message in messages:
        if message['author'] not in analysis:
            analysis[message['author']] = {}
        if message['type'] not in analysis[message['author']]:
            analysis[message['author']][message['type']] = 0

        if message['type'] == 'text':
            if 'words' not in analysis[message['author']]:
                analysis[message['author']]['words'] = 0
            analysis[message['author']]['text'] += 1
            analysis[message['author']]['words'] += len(message['content'].split())

    return analysis


def print_analysis(analysis: Dict[str, Dict[str, int]]) -> None:
    chat_members = ['Total']
    messages = []
    words = []

    for chat_member, info in analysis.items():
        chat_members.append(chat_member)
        messages.append(info['text'])
        words.append(info['words'])

    messages = [sum(messages)] + messages
    words = [sum(words)] + words

    print('/'.join(chat_members))
    print('/'.join(map(lambda x: str(x), messages)))
    print('/'.join(map(lambda x: str(x), words)))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'input_file', nargs='+',
        help="The file containing the Telegram messages to be processed, i. e. the "
             "'messages.html' obtained from exporting the chat history from within the Telegram "
             "desktop application (with option '-p') or the 'messages.csv' output file of this "
             "tool's '-p' run (with option -a). Multiple files can be passed. If both '-p' and "
             "'-a' are passed (or none of them), then the input file(s) are expected to be HTML "
             "files to be parsed and then analyzed."
    )
    parser.add_argument(
        "-n", "--name", action="append", type=str,
        help="The name of the chat partner for whom messages and words should be counted. "
             "Multiple names can be given by passing the '-n' argument multiple times. If none "
             "are given, all are counted by default."
    )
    parser.add_argument(
        "-o", "--output", default='messages.csv',
        help="The name of the output file for the messages (CSV format)."
    )
    parser.add_argument(
        "-l", "--logfile", default='telegram_analyzer.log',
        help="The name of the log file."
    )
    parser.add_argument(
        "-p", "--parse", action='store_true', default=False,
        help="Parse the passed HTML file(s) and convert them to CSV format."
    )
    parser.add_argument(
        "-a", "--analyze", action='store_true', default=False,
        help="Analyze the passed CSV files."
    )

    args = parser.parse_args()
    if not args.parse and not args.analyze:
        args.parse = True
        args.analyze = True

    logging.basicConfig(
        format='%(asctime)s-%(levelname)s:%(message)s', datefmt='%Y-%m-%dT%H:%M:%S',
        filename=args.logfile, level=logging.DEBUG
    )

    # only parse or parse and analyze: get messages from HTML
    if args.parse:
        if os.path.isfile(args.output):
            logging.exception("Error while opening CSV output file for messages. The file '{}' "
                              "already exists and will not automatically be overwritten. Move or "
                              "delete this file manually and try again.".format(args.output))
            print("Error while opening CSV output file for messages. Check log for more details.",
                  file=sys.stderr)
            sys.exit(1)

        messages = parse_message_history(args.input_file)
        save_messages_to_csv(messages, args.output)
    # only analyze: get messages from CSV
    elif args.analyze:
        messages = read_messages_from_csv(args.input_file)

    # only analyze or parse and analyze
    if args.analyze:
        print_analysis(analyze_messages(messages))
